# coursetoolslib

## install

```bash
$ python3 setup.py bdist_wheel
$ pip3 install dist/*.whl
```

## usage

### include the package in your script
```python
from coursetoolslib import (main, input, show_source_code, InputHandler)

# [...] your code [...]

if __name__=="__main__":
    # if you need to simulate input user 
    InputHandler.ADD_INPUTS(function_name="{function_name}",inputs=[{value_input_0},{value_input_1},...])

    main(locals())
```

### features

#### run a specific function

```bash
# syntax
$ python3 {your_program} --run "{function_name} args0 args1"
# example 
$ python3 tp1.py --run "exo01"
```

#### build a markdown with source code and result

```bash
# syntax
$ python3 {your_program} --doc {./path/markdown.md}
# example 
$ python3 tp1.py --doc ../output/tp1.md
```

#### run the code with input simulated
```bash 
# syntax (fill INPUTS_SIMULATED dict in your program, see the example)
$ python3 {your_program} --input_simulated
```

#### open an interpreter and read source code

```bash
# syntax
$ python3 -i {your_program} 
```
```python
# python interpreter
>>> show_source_code(function)
# example
>>> show_source_code(exo01)
```

### examples

### simple example with markdown file generated 

- python: [examples/example-001.py](examples/example-001.py)

- markdown: [examples/example-001.md](examples/example-001.md)

- interpreter test: 
```python
# python3 -i example-001.py
>>> show_source_code(exo01)
def exo01(): 
    for i in range(10): 
        print("x"*i)

>>> show_source_code(exo02)
def exo02(): 
    a = int(input("give value of a: "))
    b = int(input("give value of b: "))
    print(f"addition: a+b={a+b}")
    print(f"product: a*b={a*b}")

```
