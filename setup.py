from setuptools import find_packages, setup

setup(
    name="coursetoolslib",
    packages=find_packages(include=['coursetoolslib']),
    version="0.0.5",
    description="Course Tools lib",
    author="Hugo POUSSEUR"
)