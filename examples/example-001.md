# fichier: example-001.py

demonstration: basic example

## fonction: exo01

### code source

```python
def exo01(): 
    for i in range(10): 
        print("x"*i)

```

### sortie d'écran

> ``
> 
> `x`
> 
> `xx`
> 
> `xxx`
> 
> `xxxx`
> 
> `xxxxx`
> 
> `xxxxxx`
> 
> `xxxxxxx`
> 
> `xxxxxxxx`
> 
> `xxxxxxxxx`

<div style='page-break-after: always;'></div>

## fonction: exo02

### code source

```python
def exo02(): 
    a = int(input("give value of a: "))
    b = int(input("give value of b: "))
    print(f"addition: a+b={a+b}")
    print(f"product: a*b={a*b}")

```

### entrée utilisateur

> `"give value of a: " <<< -2`
> 
> `"give value of b: " <<< -3`

### sortie d'écran

> `addition: a+b=-5`
> 
> `product: a*b=6`

<div style='page-break-after: always;'></div>