"""demonstration: basic example"""

from coursetoolslib import (main, input, show_source_code, InputHandler)

def exo01(): 
    for i in range(10): 
        print("x"*i)

def exo02(): 
    a = int(input("give value of a: "))
    b = int(input("give value of b: "))
    print(f"addition: a+b={a+b}")
    print(f"product: a*b={a*b}")

if __name__=="__main__":
    InputHandler.ADD_INPUTS(function_name="exo02",inputs=[-2,-3])
    main(locals())