#!/usr/bin/python3

"""debug script"""

# import local 
from coursetoolslib import *
from coursetoolslib.input_handler import InputHandler, InputMode, input

def t(a=2,b=3): 
    print(a)
    print(b)
    print(a+b)
   
def t2(): 
    return None

InputHandler.SET_NEW_MODE(InputMode.SIMULATED)
InputHandler.INPUTS_SIMULATED["test"] = [2,3]

def test(): 
    print(input("inst"))
    print(input("erf"))

if __name__=="__main__": 
    # inspect
    show_source_code(t,t)

    # run 
    exec_function_from_str("t 2 3",locals())

    # main 
    main(locals())

    test()