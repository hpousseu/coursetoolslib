# import python
import inspect
from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import TerminalFormatter

# import local 
from coursetoolslib.utils import *

def get_source_code(function): 
    return inspect.getsource(function)

def print_highlight(source_code):
    content_highlight = highlight(source_code, PythonLexer(), TerminalFormatter())
    print(content_highlight)

def show_source_code(*functions): 
    for function in functions:
        source_code = get_source_code(function)
        print_highlight(source_code)

def write_source_code(function,path="./tmp.py"):
    source_code = get_source_code(function)
    write_content_to_file(path_file=path,content=source_code)