
def exec_function_from_str(function_with_args_str,locals):
    function_splits = function_with_args_str.split(" ")
    function_name = function_splits[0]
    function_args = function_splits[1:]
    local_functions = locals
    if(local_functions.get(function_name) != None):
        eval(f"{function_name}({','.join(function_args)})",locals)