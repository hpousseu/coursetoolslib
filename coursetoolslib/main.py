#import python 
import argparse

# import local
import coursetoolslib.run as run
import coursetoolslib.doc as doc
from coursetoolslib.input_handler import InputHandler, InputMode

def main(_locals): 
    parser = argparse.ArgumentParser()
    parser.add_argument("-r","--run", help="run function",type=str)
    parser.add_argument("-d","--doc", help="gen doc from code",type=str)
    parser.add_argument("-i","--input_simulated", action="store_true",help="run code with inputs simulated")
    args = parser.parse_args()

    if(args.input_simulated):
        InputHandler.SET_NEW_MODE(InputMode.SIMULATED)

    if(args.run is not None):   
        run.exec_function_from_str(args.run,_locals)

    if(args.doc is not None): 
        doc.build_doc(args.doc,_locals)