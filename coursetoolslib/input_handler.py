import builtins
import inspect
import copy

BUFFER_INSTRUCTION = ""

class InputMode: 
    USER_ASK = 0
    SIMULATED = 1

class InputHandler: 

    INPUT_MODE = InputMode.USER_ASK
    INPUTS_SIMULATED = {}
    BUFFER_INSTRUCTIONS = []

    @staticmethod
    def _INTPUT(instruction): 
        if(InputHandler.INPUT_MODE == InputMode.USER_ASK):
            return builtins.input(instruction)
        # who is calling me ? 
        curframe = inspect.currentframe()
        calframe = inspect.getouterframes(curframe, 2)
        caller_name =  calframe[1][3]

        InputHandler.BUFFER_INSTRUCTIONS.append(instruction)

        return InputHandler.INPUTS_SIMULATED[caller_name].pop(0)

    def SET_NEW_MODE(mode):
        InputHandler.INPUT_MODE = mode

    def ADD_INPUTS(function_name,inputs): 
        InputHandler.INPUTS_SIMULATED[function_name] = inputs

    def GET_LAST_INSTRUCTION(): 
        return InputHandler.BUFFER_INSTRUCTIONS.pop()

    def READ_BUFFER(): 
        # get buffer
        buffer = copy.copy(InputHandler.BUFFER_INSTRUCTIONS)
        # clear buffer
        InputHandler.BUFFER_INSTRUCTIONS = []
        return buffer


input = InputHandler._INTPUT