# import python
import sys
import os
from io import StringIO 
import copy

# import local
import coursetoolslib.inspect_code as inspect_code
import coursetoolslib.run as run
import coursetoolslib.utils as utils
from coursetoolslib.input_handler import (InputHandler, InputMode)

class Capturing(list):
    # get the output sys to variable
    # thx: https://stackoverflow.com/questions/16571150/how-to-capture-stdout-output-from-a-python-function-call
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self
    
    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio    # free up some memory
        sys.stdout = self._stdout

# get info functions

def _get_file_name(locals): 
    return locals["__file__"]

def _get_list_functions(locals): 
    local_functions = []
    for key, value in locals.items():
        if callable(value) and value.__module__ == "__main__":
            local_functions.append(key)
    return local_functions 

# md functions

def _write_md_section(depth,content): 
    return f"{'#'*depth} {content}"

def _write_md_code(code,language="python"):
    return f"```{language}\n{code}\n```"

def _write_md_result(results):
    content = []
    for result in results: 
        content.append(f"> `{result}`")
    return "\n> \n".join(content)

def _write_md_breakline():
    return "<div style='page-break-after: always;'></div>"


def build_doc(path,locals):
    md_content = []
    file_name = _get_file_name(locals)
    md_content.append(_write_md_section(depth=1,content=f"fichier: {file_name}"))
    file_doc = locals["__doc__"]
    md_content.append(file_doc)
    InputHandler.SET_NEW_MODE(InputMode.SIMULATED)
    functions = sorted(_get_list_functions(locals))
    for function_name in functions:
        md_content.append(_write_md_section(depth=2,content=f"fonction: {function_name}"))
        # section: source code
        md_content.append(_write_md_section(depth=3,content=f"code source"))
        source_code = inspect_code.get_source_code(locals[function_name])
        md_content.append(_write_md_code(code=source_code))
        # get inputs before pop 
        inputs = copy.copy(InputHandler.INPUTS_SIMULATED.get(function_name))
        # run code
        with Capturing() as result:
            run.exec_function_from_str(function_name,locals)
        # section: inputs
        if(not inputs is None): 
            buffer_instructions = InputHandler.READ_BUFFER()
            inputs_with_intructions = []
            for input_value,input_instruction in zip(inputs,buffer_instructions):
                inputs_with_intructions.append(f"\"{input_instruction}\" <<< {input_value}")
            md_content.append(_write_md_section(depth=3,content=f"entrée utilisateur"))
            md_content.append(_write_md_result(inputs_with_intructions))
        # section: output
        md_content.append(_write_md_section(depth=3,content=f"sortie d'écran"))
        md_content.append(_write_md_result(result))
        md_content.append(_write_md_breakline())
    InputHandler.SET_NEW_MODE(InputMode.USER_ASK)
    # input_simulated.set_new_mode(input_simulated.InputMode.USER_ASK)
    md_content_str = "\n\n".join(md_content)
    path_without_ext = os.path.splitext(path)[0]
    # save md 
    utils.write_content_to_file(f"{path_without_ext}.md",md_content_str)
        
    